const tableData = document.getElementById("table_data");
const nameInput = document.getElementById("name_input");
const modal = document.getElementById("modal");
const createBtn = document.getElementById("create_btn");
const modalBtnWrapper = document.getElementById("modal_btn_wrapper");
const closeBtn = document.getElementById("close_button");
const licensePlateInput = document.getElementById("license_plate_input");
const driverInput = document.getElementById("driver_input");
const vehicleInput = document.getElementById("vehicle_input");
const colorInput = document.getElementById("color_input");
const startTimeInput = document.getElementById("start_time_input");
const endTimeInput = document.getElementById("end_time_input");
const membersInput = document.getElementById("members_input");
const memberListWrapper = document.getElementById("member_list_wrapper");
const startDateInput = document.getElementById("start_date_input");
const notesInput = document.getElementById("notes_input");
const uploadCsvInput = document.getElementById("upload_csv_input")
const typeWrapper = document.getElementById("type_wrapper")


let selectedMembers = [];
let appointments = [];
let vehicles = [];
let appointmentsFromCSV = [];
let members = []
let drivers = []
let colors = []
let editMode = false;

const uploadCSV = (e) => {
  const file = e.target.files[0]

  let isConfirmed = confirm(`are you sure want to import the ${e.target.files[0].name} file`);
  if (!isConfirmed) {
    return
  }




  if (file) {
    const reader = new FileReader()

    reader.onload = async (e) => {
      const contents = e.target.result;
      const lines = contents.split("\n")
      console.log(lines);

      lines.forEach((line, i) => {
        if (i > 0) {
          const data = line.split(",")
          const colorName = data[4]
          const vehicleName = data[6];
          const timeFrom = data[3].split(" - ")[0]
          const timeTo = data[3].split(" - ")[1]
          let aptDate = data[2]
          let driverName = data[5]
          let existingMember = null

          const memberList = data[7].split("|")
          const memberIds = memberList.map((memberName, i) => {
            existingMember = members.find((member) => {
              return memberName.toLowerCase() === `${member.first_name} ${member.last_name}`.toLowerCase()
            })
            if (!existingMember) {

            }
            if (existingMember) {
              return existingMember._id
            }
          })
          memberIds.pop()

          const vehicle = vehicles.find((vehicle) => {
            return vehicleName.toLowerCase() === vehicle.license_plate_num.toLowerCase()
          })
          if (!vehicle) {
          }

          const driver = drivers.find((driver) => {
            return driverName.toLowerCase() === `${driver.first_name} ${driver.last_name}`.toLowerCase()
          })
          if (!driver) {
          }

          const color = colors.find((color) => {
            return colorName.toLowerCase() === color.category.toLowerCase()
          })
          if (!color) {
          }

          aptDate = moment(aptDate, "DD-MM-YYYY").format("YYYY-MM-DD")

          const dateFrom = moment(
            `${aptDate}T${timeFrom}`
          ).toISOString();
          const dateTo = moment(
            `${aptDate}T${timeTo}`
          ).toISOString();

          const appointment = {
            name: data[0],
            notes: data[1],
            date_from: dateFrom,
            date_to: dateTo,
            vehicle_id: vehicle._id,
            driver_id: driver._id,
            category_color_id: color._id,
            members: memberIds,
          }
          appointmentsFromCSV.push(appointment)
        }
      })

      let failedLines = ""

      await Promise.all(appointmentsFromCSV.map((appointmentInput, i) => {
        return fetch("http://localhost:3000/appointments", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify(appointmentInput),
        })
          .then((res) => {
            console.log("i", i);

            if (res.status != 200) {
              throw "Error";
            } else {
              return res.json();
            }
          })
          .then((data) => {
            if (data.success === true) {
              console.log("success");
            } else {
              throw data.message;
            }
          })
          .catch((err) => {
            failedLines += `${i + 1},`
            console.log(err);
            // window.alert(err);
          });
      }))

      console.log("failedLines", failedLines);



      if (failedLines !== "") {
        alert(`Please check data at line(s) : ${failedLines}`)
      }
      getAllData()
      uploadCsvInput.value = null
      appointmentsFromCSV = []
    }


    reader.readAsText(file)
  } else {
    alert("failed upload file")
  }
}

uploadCsvInput.addEventListener("change", uploadCSV)

closeBtn.addEventListener("click", () => {
  modal.style.display = "none";
});

createBtn.addEventListener("click", () => {
  editMode = false;
  typeWrapper.style.display = "block"
  modal.style.display = "flex";
  nameInput.value = "";
  startDateInput.value = "";
  startTimeInput.value = "";
  endTimeInput.value = "";
  notesInput.value = "";
  selectedMembers = [];
  memberListWrapper.innerHTML = "";
  modalBtnWrapper.innerHTML = `<button class="large-button" onClick="addData()">Submit Data</button>`;
});

endTimeInput.addEventListener("focusout", (e) => {
  const dateFrom = moment(
    `${startDateInput.value}T${startTimeInput.value}`
  ).toISOString();
  const dateTo = moment(
    `${startDateInput.value}T${endTimeInput.value}`
  ).toISOString();
  let bookedVehicle = null;
  let innerHTML = "";
  let filteredVehicles = [];

  appointments.forEach((appointment) => {
    console.log(
      (moment(dateFrom).isSameOrBefore(moment(appointment.date_from)) &&
        moment(dateTo).isSameOrAfter(moment(appointment.date_from))) ||
      (moment(dateFrom).isSameOrBefore(moment(appointment.date_to)) &&
        moment(dateTo).isSameOrAfter(moment(appointment.date_to)))
    );

    if (
      (moment(dateFrom).isSameOrBefore(moment(appointment.date_from)) &&
        moment(dateTo).isSameOrAfter(moment(appointment.date_from))) ||
      (moment(dateFrom).isSameOrBefore(moment(appointment.date_to)) &&
        moment(dateTo).isSameOrAfter(moment(appointment.date_to)))
    ) {
      bookedVehicle = appointment.vehicle_id._id;
    }
  });

  vehicles.forEach((vehicle) => {
    if (bookedVehicle !== null && vehicle._id !== bookedVehicle) {
      filteredVehicles.push(vehicle);
    } else if (bookedVehicle === null) {
      filteredVehicles.push(vehicle);
    }
  });

  console.log("filteredVehicles", filteredVehicles);
  console.log("bookedVehicle", bookedVehicle);

  filteredVehicles.forEach((vehicle) => {
    innerHTML += `
      <option value="${vehicle._id}" style="color:blue;">
        ${vehicle.license_plate_num} - 
        ${vehicle.category_color_id && vehicle.category_color_id.category}
      </option>
    `;
  });

  vehicleInput.innerHTML = innerHTML;
});

const addData = () => {
  const memberIds = selectedMembers.map((member) => {
    return member.id;
  });

  if (nameInput.value === "") {
    return window.alert("Name of event has not been filled");
  }

  if (
    startDateInput.value === "" ||
    endTimeInput.value === "" ||
    startTimeInput.value === ""
  ) {
    return window.alert("Time and date of event has not been filled");
  }

  if (!selectedMembers || selectedMembers.length === 0) {
    return window.alert("Please select at least one member");
  }

  const dateFrom = moment(
    `${startDateInput.value}T${startTimeInput.value}`
  ).toISOString();
  const dateTo = moment(
    `${startDateInput.value}T${endTimeInput.value}`
  ).toISOString();

  if (moment(dateFrom).isBefore(moment())) {
    return alert("not allowed create past appointment")
  }

  console.log("input payload", {
    vehicle_id: vehicleInput.value,
    driver_id: driverInput.value,
    category_color_id: colorInput.value,
    notes: notesInput.value,
    name: nameInput.value,
    date_from: dateFrom,
    date_to: dateTo,
    members: memberIds,
  });

  fetch("http://localhost:3000/appointments", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      vehicle_id: vehicleInput.value,
      driver_id: driverInput.value,
      category_color_id: colorInput.value,
      notes: notesInput.value,
      name: nameInput.value,
      date_from: dateFrom,
      date_to: dateTo,
      members: memberIds,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      console.log(data);

      if (data.success === true) {

        modal.style.display = "none";
        getAllData();
        // if (data.successWithSomeError) {
        //   alert("Create appointments success with some errors")
        // }
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

const getAllData = () => {
  fetch("http://localhost:3000/appointments")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;
      if (data.appointments && data.appointments.length > 0) {
        appointments = data.appointments;
        data.appointments.forEach((appointment) => {
          innerHTML += `
        <tr>
            <td>${appointment.name || "-"}</td>
            <td>${
            (appointment.category_color_id &&
              appointment.category_color_id.category) ||
            "-"
            }</td>
            <td>${moment(appointment.date_from).format("DD-MM-YYYY") || "-"}
            </td>
              <td>${
            `${moment(appointment.date_from).format("hh:mm A")} - ${moment(
              appointment.date_to
            ).format("hh:mm A")}` || "-"
            }</td>
            <td>${
            (appointment.vehicle_id &&
              appointment.vehicle_id.category_color_id &&
              appointment.vehicle_id.category_color_id.category) ||
            "-"
            }</td>
            <td>${
            (appointment.vehicle_id &&
              appointment.vehicle_id.license_plate_num) ||
            "-"
            }</td>
            <td>${
            (appointment.driver_id &&
              `${appointment.driver_id.first_name} ${appointment.driver_id.last_name}`) ||
            "-"
            }</td>
          <td>${appointment.notes || "-"}</td>
          <td class="bn bw"> 
          <button class="large-button bc-green" onClick="editMode = true;openDetail('${
            appointment._id
            }'); ">Edit</button>
              <button class="large-button bc-red" onClick="deleteData('${
            appointment._id
            }')">Delete</button>
          </td>
        </tr>
        `;
        });
      } else {
        innerHTML = `
            <tr>
              <td colspan="8" style="text-align: center;">
                No Data ...
              </td>
            </tr>
        `;
      }
      tableData.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

const openDetail = (id) => {
  if (editMode) {
    typeWrapper.style.display = "none"
  } else {
    typeWrapper.style.display = "block"
  }
  fetch(`http://localhost:3000/appointments/${id}`)
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.appointment) {
        const memberIds = data.appointment.members.map((member) => {
          return {
            id: member._id,
            name: `${member.first_name} ${member.last_name}`,
          };
        });
        selectedMembers = memberIds;
        let innerHTML = "";
        memberIds.forEach((member) => {
          innerHTML += `<div class="member">${member.name} <span class="remove_member_button" onClick="removeSelectedMember('${member.id}')">X</span></div>`;
        });
        memberListWrapper.innerHTML = innerHTML;
        const dateFrom = moment(data.appointment.date_from).format(
          "YYYY-MM-DD"
        );
        const timeFrom = moment(data.appointment.date_from).format("HH:mm");
        const dateTo = moment(data.appointment.date_to).format("YYYY-MM-DD");
        const timeTo = moment(data.appointment.date_to).format("HH:mm");

        vehicleInput.value = data.appointment.vehicle_id._id;
        driverInput.value = data.appointment.driver_id._id;
        colorInput.value = data.appointment.category_color_id._id;
        nameInput.value = data.appointment.name;
        startDateInput.value = dateFrom;
        startTimeInput.value = timeFrom;
        endTimeInput.value = timeTo;
        memberIds.value = memberIds;
        notesInput.value = data.appointment.notes;

        modalBtnWrapper.innerHTML = `
          <button class="large-button bc-green" onClick="editData('${data.appointment._id}')">Edit</button>
        `;
        modal.style.display = "flex";
      }
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

const editData = (id) => {
  const memberIds = selectedMembers.map((member) => {
    return member.id;
  });

  if (nameInput.value === "") {
    return window.alert("Name of event has not been filled");
  }

  if (
    startDateInput.value === "" ||
    endTimeInput.value === "" ||
    startTimeInput.value === ""
  ) {
    return window.alert("Time and date of event has not been filled");
  }

  if (!selectedMembers || selectedMembers.length === 0) {
    return window.alert("Please select at least one member");
  }

  const dateFrom = moment(
    `${startDateInput.value}T${startTimeInput.value}`
  ).toISOString();
  const dateTo = moment(
    `${startDateInput.value}T${endTimeInput.value}`
  ).toISOString();

  fetch(`http://localhost:3000/appointments/${id}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      vehicle_id: vehicleInput.value,
      driver_id: driverInput.value,
      category_color_id: colorInput.value,
      notes: notesInput.value,
      name: nameInput.value,
      date_from: dateFrom,
      date_to: dateTo,
      members: memberIds,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

const deleteData = (id) => {
  fetch(`http://localhost:3000/appointments/${id}`, {
    method: "DELETE",
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

const getAllDrivers = () => {
  fetch("http://localhost:3000/drivers")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;
      if (data.drivers && data.drivers.length > 0) {
        drivers = data.drivers
        data.drivers.forEach((driver) => {
          innerHTML += `
            <option value="${driver._id}" style="color:blue;">${driver.first_name} ${driver.last_name}</option>
          `;
        });
      }
      driverInput.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

const getAllVehicles = () => {
  fetch("http://localhost:3000/vehicles")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;

      if (data.vehicles && data.vehicles.length > 0) {
        vehicles = data.vehicles;
        data.vehicles.forEach((vehicle) => {
          innerHTML += `
            <option value="${vehicle._id}" style="color:blue;">
              ${vehicle.license_plate_num} - 
              ${vehicle.category_color_id && vehicle.category_color_id.category}
            </option>
          `;
        });
      }

      vehicleInput.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

/* 
  Function to get all category colors data from API
*/
const getAllColors = () => {
  fetch("http://localhost:3000/category_colors")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;
      if (data.colors && data.colors.length > 0) {
        colors = data.colors
        /* 
          If we get all data from API, 
          transform list of all data to User Interface (HTML) using 
          for Each loop
        */

        data.colors.forEach((color) => {
          if (color.type === "appointment") {
            innerHTML += `
            <option value="${color._id}" style="color:blue;">${color.category}</option>
          `;
          }
        });
      }
      // Insert the generated UI of table data to table data on HTML file
      colorInput.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

const getAllMembers = () => {
  fetch("http://localhost:3000/members")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;
      if (data.members && data.members.length > 0) {
        members = data.members

        data.members.forEach((member) => {
          innerHTML += `
            <option value="${member._id}-*-${member.first_name} ${member.last_name}">${member.first_name} ${member.last_name}</option>
          `;
        });
      }
      innerHTML += `<option disabled selected value> Please select one or more </option>`;
      membersInput.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

const addSelectedMember = () => {
  let innerHTML = "";
  const id = membersInput.value.split("-*-")[0];
  const name = membersInput.value.split("-*-")[1];
  const isMemberExist = selectedMembers.find((member) => member.id === id);

  if (isMemberExist) {
    return;
  }
  selectedMembers.push({
    id,
    name,
  });
  selectedMembers.forEach((member) => {
    innerHTML += `<div class="member">${member.name} <span class="remove_member_button" onClick="removeSelectedMember('${member.id}')">X</span></div>`;
  });

  memberListWrapper.innerHTML = innerHTML;
};

const removeSelectedMember = (id) => {
  let innerHTML = "";
  selectedMembers = selectedMembers.filter((member) => {
    if (member.id !== id) {
      return true;
    }
  });

  selectedMembers.forEach((member) => {
    innerHTML += `<div class="member">${member.name} <span class="remove_member_button" onClick="removeSelectedMember('${member.id}')">X</span></div>`;
  });
  memberListWrapper.innerHTML = innerHTML;
};

getAllData();
getAllDrivers();
getAllVehicles();
getAllColors();
getAllMembers();

const exportAppointment = () => {
  let isConfirmed = confirm(`are you sure want to export all appointment`);
  if (!isConfirmed) {
    return
  }
  let rows = appointments.map((appointment) => {
    const appointmentDate = moment(appointment.date_from).format("DD-MM-YYYY")
    const appointmentTime = moment(appointment.date_from).format("hh:mm A")
      + " " +
      moment(
        appointment.date_to
      ).format("hh:mm A")
    let members = appointment.members.map(member => `${member.first_name} ${member.last_name}`)
    members = members.join("|")
    members += "|"
    return [appointment.name,
    appointment.notes,
      appointmentDate,
      appointmentTime,
    appointment.category_color_id && appointment.category_color_id.category,
    (appointment.driver_id &&
      `${appointment.driver_id.first_name} ${appointment.driver_id.last_name}`) ||
    "-",
    (appointment.vehicle_id &&
      appointment.vehicle_id.license_plate_num) ||
    "-",
      members
    ]
  })

  rows = [["Name", "Notes", "Date", "Time", "Type", "Driver", "Vehicle", "Members"], ...rows]



  let csvContent = "data:text/csv;charset=utf-8,"
    + rows.map(e => e.join(",")).join("\n");
  var encodedUri = encodeURI(csvContent);
  var link = document.createElement("a");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", "appointments.csv");
  document.body.appendChild(link); // Required for FF

  link.click(); // This will download the data file named "my_data.csv".
}


