const calendarWrapper = document.getElementById("calendar_wrapper");
const closeBtn = document.getElementById("close_button");
const tableData = document.getElementById("table_data");
const month = document.getElementById("month");
let appointments = [];
let currDate = new Date();

closeBtn.addEventListener("click", () => {
    modal.style.display = "none";
});

const showNextMonth = () => {
    currDate.setMonth(currDate.getMonth() + 1);
    generateCalendar();
    getAppointmentByMonth(currDate);
};

const showPrevMonth = () => {
    currDate.setMonth(currDate.getMonth() - 1);
    generateCalendar();
    getAppointmentByMonth(currDate);
};

const generateCalendar = () => {
    let currMonth = currDate.getMonth();
    let calendarWrapperInnerHTML = calendarWrapper.innerHTML;
    const monthList = {
        0: "January",
        1: "February",
        2: "March",
        3: "April",
        4: "May",
        5: "June",
        6: "July",
        7: "August",
        8: "September",
        9: "October",
        10: "November",
        11: "December",
    };
    const currYear = currDate.getFullYear();
    month.innerHTML = `${monthList[currMonth]} ${currYear}`;
    const lastDayInMonth = new Date(currYear, currMonth, 0).getDate();
    const firstDayInMonth = new Date(currYear, currMonth, 1).getDay() + 1;

    let dayCounter = 1;
    calendarWrapper.innerHTML = "";
    calendarWrapper.innerHTML += `
        <div class="day_info_box">Sunday</div>
        <div class="day_info_box">Monday</div>
        <div class="day_info_box">Tuesday</div>
        <div class="day_info_box">Wednesday</div>
        <div class="day_info_box">Thursday</div>
        <div class="day_info_box">Friday</div>
        <div class="day_info_box">Saturday</div>
    `;
    for (i = 1; i <= 35; i++) {
        if (i >= firstDayInMonth && dayCounter <= lastDayInMonth) {
            calendarWrapper.innerHTML += `
                <div class="calendar_box" id="day_${dayCounter}">
                    ${dayCounter}
                    <div class="appointment_list" id="appointment_${dayCounter}" onClick="openAppointmentsModal('${dayCounter}')">
                    </div>
                </div>
            `;
            dayCounter++;
        } else {
            calendarWrapper.innerHTML += `
                <div class="calendar_box">${" "}</div>
            `;
        }
    }
};

const getAppointmentByMonth = (currDate) => {
    const currMonth = currDate.getMonth() + 1;
    const currYear = currDate.getFullYear();


    fetch(
        `http://localhost:3000/appointments/get_appointment_by_month/${currMonth}/${currYear}`
    )
        .then((res) => {
            if (res.status != 200) {
                throw "Error";
            } else {
                return res.json();
            }
        })
        .then((data) => {
            if (data.data) {
                appointments = data.data;
                data.data.forEach((appointment) => {
                    const date = moment(appointment.date_from).date();

                    const appointmentDiv = document.getElementById(`appointment_${date}`);
                    appointmentDiv.innerHTML += `<div class="appointment_name"> - ${appointment.name}</div>`;
                });
            }
        })
        .catch((err) => {
            window.alert(err.message);
        });
};

const openAppointmentsModal = (day) => {
    tableData.innerHTML = "";
    appointments.forEach((appointment) => {
        if (moment(appointment.date_from).date().toString() === day) {
            let membersInnerHTML = "";
            appointment.members.forEach((member) => {
                membersInnerHTML += `<div>${member.first_name} ${member.last_name}</div>`;
            });

            tableData.innerHTML += `<tr>
                <td>
                ${
                (appointment.vehicle_id &&
                    appointment.vehicle_id[0] &&
                    appointment.vehicle_id[0].license_plate_num) ||
                "-"
                }
                </td>
                <td>
                ${
                (appointment.driver_id &&
                    appointment.driver_id[0] &&
                    appointment.driver_id[0].first_name +
                    " " +
                    appointment.driver_id[0].last_name) ||
                "-"
                }
                </td>
                <td>
                    ${appointment.name}
                </td>
                <td>
                    ${moment(appointment.date_from).format("DD-MM-YYYY") || "-"}
                </td>
                <td>${
                `${moment(appointment.date_from).format(
                    "hh:mm A"
                )} - ${moment(appointment.date_to).format("hh:mm A")}` || "-"
                }</td>
                <td>
                    ${appointment.notes}
                </td>
                <td>
                    ${membersInnerHTML}
                </td>
                <td class="bn bw">
                    <img class="print_icon" src="./assets/img/print-icon.png" alt="print" onClick="printOnNewTab('${
                appointment._id
                }')">
                </td>
            </tr>`;
        }
    });
    modal.style.display = "flex";
};

const printOnNewTab = (id) => {
    console.log("printOnNewTab");
    window.open(
        `http://localhost:5500/print_appointment.html?id=${id}`,
        "_blank"
    );
};

generateCalendar();
getAppointmentByMonth(currDate);
