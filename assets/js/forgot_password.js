const resetPassBtn = document.getElementById("resetPass_btn");
const emailInput = document.getElementById("email_input");

// Call the reset password API if the user click the reset button
resetPassBtn.addEventListener("click", () => {
  // Call the reset password API
  fetch("http://localhost:3000/employees/reset_password", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: emailInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        // If the API return the success response, redirect to login page
        window.location.href = "http://localhost:5500/index.html";
      }
    })
    .catch((err) => {
      // If the API return the error response, show error alert
      window.alert(`Error`);
    });
});
