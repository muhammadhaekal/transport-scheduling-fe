const nameInput = document.getElementById("name_input");
const emailInput = document.getElementById("email_input");
const passwordInput = document.getElementById("password_input");
const birthDateInput = document.getElementById("birth_date_input");
const birthPlaceInput = document.getElementById("birth_place_input");
const jobTitleInput = document.getElementById("job_title_input");
const submitBtn = document.getElementById("submit_btn");

// Call insert employee API if user click the submit button
submitBtn.addEventListener("click", () => {
  // Get all input from user
  // console.log("nameInput", nameInput.value);
  // console.log("emailInput", emailInput.value);
  // console.log("passwordInput", passwordInput.value);
  // console.log("birthDateInput", birthDateInput.value);
  // console.log("birthPlaceInput", birthPlaceInput.value);
  // console.log("jobTitleInput", jobTitleInput.value);

  // Send all input from user to Back End
  fetch("http://localhost:3000/employees", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      name: nameInput.value,
      email: emailInput.value,
      password: passwordInput.value,
      birth_date: birthDateInput.value,
      birth_place: birthPlaceInput.value,
      job_title: jobTitleInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        // If the API return success response, redirect page to login page
        window.location.href = "http://localhost:5500/index.html";
      }
    })
    .catch((err) => {
      // If the API return error response, show error alert
      window.alert(`Error`);
    });
});
