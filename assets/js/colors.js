// Select every important element from HTML using HTML id
const tableData = document.getElementById("table_data");
const categoryInput = document.getElementById("category_input");
const hexColorInput = document.getElementById("hex_color_input");
const modal = document.getElementById("modal");
const createBtn = document.getElementById("create_btn");
const modalBtnWrapper = document.getElementById("modal_btn_wrapper");
const closeBtn = document.getElementById("close_button");
const vehicleRadioBtn = document.getElementById("vehicle_radio_btn");
const appointmentRadioBtn = document.getElementById("appointment_radio_btn");
const hexColorInputLabel = document.getElementById("hex_color_input_label");

// Change list of category option if use click vehicle radio button
vehicleRadioBtn.addEventListener("click", () => {
  categoryInput.innerHTML = `
    <option value="bus" selected="selected">Bus</option>
    <option value="car">Car</option>
    <option value="volunteer owned">Volunteer Owned</option>
  `;
  hexColorInput.style.display = "inline-block";
  hexColorInputLabel.style.display = "inline-block";
});

// Change list of category option if use click appointment radio button
appointmentRadioBtn.addEventListener("click", () => {
  categoryInput.innerHTML = `
    <option value="onetime" selected="selected">One Time</option>
    <option value="daily">Daily</option>
    <option value="weekly">Weekly</option>
    <option value="fortnightly">Fortnightly</option>
    <option value="monthly">Monthly</option>
    <option value="yearly">Yearly</option>
  `;
  hexColorInput.style.display = "none";
  hexColorInputLabel.style.display = "none";
});

// Hide modal if the user click the close button
closeBtn.addEventListener("click", () => {
  modal.style.display = "none";
});

// Display modal with empty input value if the user click create button
createBtn.addEventListener("click", () => {
  modal.style.display = "flex";
  categoryInput.value = "";
  hexColorInput.value = "";
  vehicleRadioBtn.checked = false;
  appointmentRadioBtn.checked = false;

  modalBtnWrapper.innerHTML = `<button class="large-button" onClick="addColor()">Submit Data</button>`;
});

/* 
  Function to send all input from user (inside modal UI)
  to API using fetch
*/
const addColor = () => {
  fetch("http://localhost:3000/category_colors", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      category: categoryInput.value,
      type: vehicleRadioBtn.checked ? "vehicle" : "appointment",
      hex_color: appointmentRadioBtn.checked
        ? Math.random().toString()
        : hexColorInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        /*  
          if we get success response from API,
          close the modal and call getAllColors() function
        */
        modal.style.display = "none";
        getAllColors();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      // If we get error from API, show window alert
      if (
        err.includes(
          `E11000 duplicate key error collection: myapp.category_colors index: category_1 dup`
        )
      ) {
        err = "This category already exist";
      } else if (
        err.includes(
          "E11000 duplicate key error collection: myapp.category_colors index: hex_color_1 dup"
        )
      ) {
        err = "Please choose another color";
      }
      window.alert(err);
    });
};

/* 
  Function to get all category colors data from API
*/
const getAllColors = () => {
  fetch("http://localhost:3000/category_colors")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;
      if (data.colors && data.colors.length > 0) {
        /* 
          If we get all data from API, 
          transform list of all data to User Interface (HTML) using 
          for Each loop
        */
        data.colors.forEach((color) => {
          if (color.type === "appointment") {
            color.hex_color = "";
          }
          innerHTML += `
        <tr>
            <td>${color.category}</td>
            <td>${color.type}</td>
            <td >
                ${color.hex_color} 
                <div class="color_circle" style="background-color:${color.hex_color}"></div>
            </td>
            <td class="bn bw"> 
              <button class="large-button bc-green" onClick="openDetail('${color._id}')">Edit</button>
              <button class="large-button bc-red" onClick="deleteColor('${color._id}')">Delete</button>
            </td>
        </tr>
        `;
        });
      } else {
        innerHTML = `
            <tr>
              <td colspan="3" style="text-align: center;">
                No Data ...
              </td>
            </tr>
        `;
      }
      // Insert the generated UI of table data to table data on HTML file
      tableData.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

/* 
  Function to get detail of category color by id
  and put the detail data to modal
*/
const openDetail = (id) => {
  fetch(`http://localhost:3000/category_colors/${id}`)
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.color) {
        // If we get the data, insert the detail data to modal UI
        if (data.color.type === "vehicle") {
          categoryInput.innerHTML = `
            <option value="bus" selected="selected">Bus</option>
            <option value="car">Car</option>
            <option value="volunteer owned">Volunteer Owned</option>
          `;
        } else {
          categoryInput.innerHTML = `
            <option value="onetime" selected="selected">One Time</option>
            <option value="daily">Daily</option>
            <option value="weekly">Weekly</option>
            <option value="fortnightly">Fortnightly</option>
            <option value="monthly">Monthly</option>
            <option value="yearly">Yearly</option>
          `;
          hexColorInput.style.display = "none";
          hexColorInputLabel.style.display = "none";
        }
        vehicleRadioBtn.checked = data.color.type === "vehicle" ? true : false;
        appointmentRadioBtn.checked =
          data.color.type === "appointment" ? true : false;
        categoryInput.value = data.color.category;
        hexColorInput.value = data.color.hex_color;
      }

      modal.style.display = "flex";
      // Insert Edit and Delete button to modal
      modalBtnWrapper.innerHTML = `
        <button class="large-button bc-green" onClick="editColor('${data.color._id}')">Edit</button>
      `;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

/* 
  Function to edit category color data
*/
const editColor = (id) => {
  fetch(`http://localhost:3000/category_colors/${id}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      category: categoryInput.value,
      type: vehicleRadioBtn.checked ? "vehicle" : "appointment",
      hex_color: appointmentRadioBtn.checked
        ? Math.random().toString()
        : hexColorInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllColors();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

/* 
  Function to delete category color data
*/
const deleteColor = (id) => {
  fetch(`http://localhost:3000/category_colors/${id}`, {
    method: "DELETE",
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllColors();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

// Initialize getAllColors function after the user open the page
getAllColors();
