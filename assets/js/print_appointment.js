// window.print();

const appointmentName = document.getElementById("appointment_name")
const appointmentDate = document.getElementById("appointment_date")
const appointmentTime = document.getElementById("appointment_time")
const appointmentNotes = document.getElementById("appointment_notes")
const membersWrapper = document.getElementById("members_wrapper")

const openDetail = () => {
    const params = new URLSearchParams(window.location.search)
    const id = params.get('id')
    fetch(`http://localhost:3000/appointments/${id}`)
        .then((res) => {
            if (res.status != 200) {
                throw "Error";
            } else {
                return res.json();
            }
        })
        .then((data) => {
            if (data.appointment) {
                let membersWrapperInnerHTML = ''
                data.appointment.members.forEach(member => {
                    membersWrapperInnerHTML += `
                        <h5>${member.last_name}, ${member.first_name}</h5>
                        <h5>Phone Number : ${member.phone_number}</h5>
                        <h5>Address : ${member.address}</h5>
                    `
                })
                const date = moment(data.appointment.date_from).format(
                    "YYYY-MM-DD"
                );
                const time = moment(data.appointment.date_from).format("hh:mm A") + "-" + moment(data.appointment.date_to).format("hh:mm A")
                appointmentName.innerHTML = data.appointment.name
                appointmentDate.innerHTML = `Date : ${date}`
                appointmentTime.innerHTML = `Time : ${time}`
                appointmentNotes.innerHTML = `Notes : ${data.appointment.notes}`
                membersWrapper.innerHTML = membersWrapperInnerHTML

                window.print()
            }
        })
        .catch((err) => {
            window.alert(err.message);
        });
};

openDetail()