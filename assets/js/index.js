// Select every important element from HTML using HTML id
const emailInput = document.getElementById("email_input");
const passwordInput = document.getElementById("password_input");
const submitBtn = document.getElementById("submit_btn");
const signupBtn = document.getElementById("signup_btn");
const forgotPassBtn = document.getElementById("forgotPass_btn");

// Send all input data from user if user click the submit button
submitBtn.addEventListener("click", () => {
  // Send all input from user to Back End
  fetch("http://localhost:3000/employees/login", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: emailInput.value,
      password: passwordInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        // If the API return success message, redirect to dashboard page
        window.location.href = "http://localhost:5500/dashboard.html";
      }
    })
    .catch((err) => {
      // If the API return error show window alert
      window.alert(`Error`);
    });
});

// Redirect to signup page if the user click signup link
signupBtn.addEventListener("click", () => {
  // Redirect to signup page
  window.location.href = "http://localhost:5500/signup.html";
});

// Redirect to forgot password page if the user click forgon password link
forgotPassBtn.addEventListener("click", () => {
  // Redirect to reset password page
  window.location.href = "http://localhost:5500/forgot_password.html";
});
