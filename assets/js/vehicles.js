const tableData = document.getElementById("table_data");
const nameInput = document.getElementById("name_input");
const modal = document.getElementById("modal");
const createBtn = document.getElementById("create_btn");
const modalBtnWrapper = document.getElementById("modal_btn_wrapper");
const closeBtn = document.getElementById("close_button");
const colorInput = document.getElementById("color_input");
const licensePlateInput = document.getElementById("license_plate_input");

// Close modal if the user click close button
closeBtn.addEventListener("click", () => {
  //css functionn to hide UI modal
  modal.style.display = "none";
});

// Show modal if the user click create button
createBtn.addEventListener("click", () => {
  //flex: css function to display UI modal
  modal.style.display = "flex";
  //to refresh input to be blank for new data
  licensePlateInput.value = "";
  //to create submit button
  modalBtnWrapper.innerHTML = `<button class="large-button" onClick="addData()">Submit Data</button>`;
});

/* 
  Function to get all category colors data from API
*/
const getAllColors = () => {
  fetch("http://localhost:3000/category_colors")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;
      if (data.colors && data.colors.length > 0) {
        /* 
          If we get all data from API, 
          insert all colors into select option tag
        */
        data.colors.forEach((color) => {
          if (color.type === "vehicle") {
            innerHTML += `
            <option value="${color._id}" style="color:blue;">${color.category}</option>
          `;
          }
        });
      }
      // Insert the generated UI of table data to table data on HTML file
      colorInput.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

/* 
  Function to add vehicle data
*/
const addData = () => {
  if (!colorInput.value || !licensePlateInput.value) {
    return alert("please fill all information")
  }

  fetch("http://localhost:3000/vehicles", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      category_color_id: colorInput.value,
      license_plate_num: licensePlateInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        // If the API return success message,
        // close the
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

/* 
  Get all vehicle data from API
*/
const getAllData = () => {
  fetch("http://localhost:3000/vehicles")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;
      if (data.vehicles && data.vehicles.length > 0) {
        data.vehicles.forEach((vehicle) => {
          innerHTML += `
        <tr>
            <td>${
            (vehicle.category_color_id &&
              vehicle.category_color_id.category) ||
            "-"
            }</td>
            <td>
                ${
            (vehicle.category_color_id &&
              vehicle.category_color_id.hex_color) ||
            "-"
            } 
                <div class="color_circle" style="background-color:${
            (vehicle.category_color_id &&
              vehicle.category_color_id.hex_color) ||
            "-"
            }"></div>
            </td>
            <td class="ucase">${vehicle.license_plate_num}</td>
            <td class="bn bw"> 
              <button class="large-button bc-green" onClick="openDetail('${
            vehicle._id
            }')">Edit</button>
              <button class="large-button bc-red" onClick="deleteData('${
            vehicle._id
            }')">Delete</button>
          </td>
        </tr>
        `;
        });
      } else {
        innerHTML = `
            <tr>
              <td colspan="3" style="text-align: center;">
                No Data ...
              </td>
            </tr>
        `;
      }
      tableData.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

/* 
  Function to get one vehichle data by id
*/
const openDetail = (id) => {
  fetch(`http://localhost:3000/vehicles/${id}`)
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.vehicle) {
        colorInput.value = data.vehicle.category_color_id;
        licensePlateInput.value = data.vehicle.license_plate_num;
      }
      modal.style.display = "flex";
      modalBtnWrapper.innerHTML = `
        <button class="large-button bc-green" onClick="editData('${data.vehicle._id}')">Edit</button>
      `;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

/* 
  Function to edit one vehichle data by id
*/
const editData = (id) => {
  fetch(`http://localhost:3000/vehicles/${id}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      category_color_id: colorInput.value,
      license_plate_num: licensePlateInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        // If the API return success response,
        // Hide input modal and get all data
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      // Show error alert if the API return error message
      window.alert(err);
    });
};

/* 
  Function to delete one vehichle data by id
*/
const deleteData = (id) => {
  fetch(`http://localhost:3000/vehicles/${id}`, {
    method: "DELETE",
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        // If the API return success message,
        // Hide the input modal and get all data
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

// Get all vehicle data and color after user open the page
getAllData();
getAllColors();
