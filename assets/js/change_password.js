const submitBtn = document.getElementById("submit_btn");
const passwordInput = document.getElementById("password_input");
const confirmPasswordInput = document.getElementById("confirm_password_input");

// Call the change password API if the user click the submit button
submitBtn.addEventListener("click", () => {
  const urlParams = new URLSearchParams(window.location.search);
  const email = urlParams.get("email");

  // Call change passwor API using fetch
  fetch("http://localhost:3000/employees/change_password", {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email,
      password: passwordInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        // If the API return the success response, redirect to login page
        window.location.href = "http://localhost:5500/index.html";
      }
    })
    .catch((err) => {
      // If the API return error response, show Error alert
      window.alert(`Error`);
    });
});
