const tableData = document.getElementById("table_data");
const firstName = document.getElementById("first_name_input");
const lastName = document.getElementById("last_name_input");
const phoneNumInput = document.getElementById("phone_num_input");
const modal = document.getElementById("modal");
const createBtn = document.getElementById("create_btn");
const modalBtnWrapper = document.getElementById("modal_btn_wrapper");
const closeBtn = document.getElementById("close_button");

closeBtn.addEventListener("click", () => {
  modal.style.display = "none";
});

createBtn.addEventListener("click", () => {
  modal.style.display = "flex";

  firstName.value = "";
  lastName.value = "";
  phoneNumInput.value = "";

  modalBtnWrapper.innerHTML = `<button class="large-button" onClick="addData()">Submit Data</button>`;
});

const addData = () => {
  if (!firstName.value || !lastName.value || !phoneNumInput.value) {
    return alert("please fill all information")
  }

  fetch("http://localhost:3000/drivers", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      first_name: firstName.value,
      last_name: lastName.value,
      phone_num: phoneNumInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

const getAllData = () => {
  fetch("http://localhost:3000/drivers")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;
      let age;
      if (data.drivers && data.drivers.length > 0) {
        data.drivers.forEach((driver) => {
          innerHTML += `
        <tr>
            <td>${driver.first_name}</td>
            <td>${driver.last_name}</td>
            <td>${driver.phone_num}</td>
            <td class="bn bw"> 
              <button class="large-button bc-green" onClick="openDetail('${driver._id}')">Edit</button>
            <button class="large-button bc-red" onClick="deleteData('${driver._id}')">Delete</button>
            </td>
        </tr>
        `;
        });
      } else {
        innerHTML = `
            <tr>
              <td colspan="3" style="text-align: center;">
                No Data ...
              </td>
            </tr>
        `;
      }
      tableData.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

const openDetail = (id) => {
  fetch(`http://localhost:3000/drivers/${id}`)
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.driver) {
        firstName.value = data.driver.first_name;
        lastName.value = data.driver.last_name;
        phoneNumInput.value = data.driver.phone_num;
      }
      modal.style.display = "flex";
      modalBtnWrapper.innerHTML = `
        <button class="large-button bc-green" onClick="editData('${data.driver._id}')">Edit</button>
        <button class="large-button bc-red" onClick="deleteData('${data.driver._id}')">Delete</button>
      `;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

const editData = (id) => {
  fetch(`http://localhost:3000/drivers/${id}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      first_name: firstName.value,
      last_name: lastName.value,
      phone_num: phoneNumInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

const deleteData = (id) => {
  fetch(`http://localhost:3000/drivers/${id}`, {
    method: "DELETE",
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

getAllData();
