const tableData = document.getElementById("table_data");
const firstNameInput = document.getElementById("first_name_input");
const lastNameInput = document.getElementById("last_name_input");
const addressInput = document.getElementById("address_input");
const phoneNumberInput = document.getElementById("phone_number_input");
const modal = document.getElementById("modal");
const createBtn = document.getElementById("create_btn");
const modalBtnWrapper = document.getElementById("modal_btn_wrapper");
const closeBtn = document.getElementById("close_button");

closeBtn.addEventListener("click", () => {
  modal.style.display = "none";
});

createBtn.addEventListener("click", () => {
  modal.style.display = "flex";

  firstNameInput.value = "";
  lastNameInput.value = "";
  phoneNumberInput.value = "";
  addressInput.value = "";

  modalBtnWrapper.innerHTML = `<button class="large-button" onClick="addData()">Submit Data</button>`;
});

const addData = () => {
  if (!firstNameInput.value || !lastNameInput.value || !phoneNumberInput.value || !addressInput.value) {
    return alert("please fill all information")
  }

  fetch("http://localhost:3000/members", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      first_name: firstNameInput.value,
      last_name: lastNameInput.value,
      phone_number: phoneNumberInput.value,
      address: addressInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

const getAllData = () => {
  fetch("http://localhost:3000/members")
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      let innerHTML = ``;
      if (data.members && data.members.length > 0) {
        data.members.forEach((member) => {
          innerHTML += `
        <tr>
            <td>${member.first_name}</td>
            <td>${member.last_name}</td>
            <td>${member.phone_number}</td>
            <td>${member.address}</td>
            <td class="bn bw"> 
              <button class="large-button bc-green" onClick="openDetail('${member._id}')">Edit</button>
              <button class="large-button bc-red" onClick="deleteColor('${member._id}')">Delete</button>
          </td>
        </tr>
        `;
        });
      } else {
        innerHTML = `
            <tr>
              <td colspan="4" style="text-align: center;">
                No Data ...
              </td>
            </tr>
        `;
      }
      tableData.innerHTML = innerHTML;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

const openDetail = (id) => {
  fetch(`http://localhost:3000/members/${id}`)
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.member) {
        firstNameInput.value = data.member.first_name;
        lastNameInput.value = data.member.last_name;
        phoneNumberInput.value = data.member.phone_number;
        addressInput.value = data.member.address;
      }
      modal.style.display = "flex";
      modalBtnWrapper.innerHTML = `
        <button class="large-button bc-green" onClick="editColor('${data.member._id}')">Submit</button>
      `;
    })
    .catch((err) => {
      window.alert(err.message);
    });
};

const editColor = (id) => {
  fetch(`http://localhost:3000/members/${id}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      first_name: firstNameInput.value,
      last_name: lastNameInput.value,
      phone_number: phoneNumberInput.value,
      address: addressInput.value,
    }),
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

const deleteColor = (id) => {
  fetch(`http://localhost:3000/members/${id}`, {
    method: "DELETE",
  })
    .then((res) => {
      if (res.status != 200) {
        throw "Error";
      } else {
        return res.json();
      }
    })
    .then((data) => {
      if (data.success === true) {
        modal.style.display = "none";
        getAllData();
      } else {
        throw data.message;
      }
    })
    .catch((err) => {
      window.alert(err);
    });
};

getAllData();
